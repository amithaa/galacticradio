﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateVisuals : MonoBehaviour {


	public GameObject samplePrefab;

	List<GameObject> listOfVisuals = new List<GameObject>();
	List<GameObject> listOfreflectVisuals = new List<GameObject>();
	List<GameObject> listOfreflectVisuals2 = new List<GameObject>();
	List<GameObject> listOfreflectVisuals3 = new List<GameObject>();


	public float maxScale;
	public float maxScale2;

	public TrailRenderer tr;

	// Use this for initialization
	void Start () {

		for (int i = 0; i < 300	; ++i) {
			GameObject sampleCube = (GameObject)Instantiate (samplePrefab,this.transform.position,this.transform.rotation,this.transform);
			sampleCube.name = "SampleCube" + i;
//			this.transform.eulerAngles = new Vector3 (0,-0.703125f*i,0);
//			sampleCube.transform.position = Vector3.right * 100;
			sampleCube.transform.position = new Vector3(sampleCube.transform.position.x+i,sampleCube.transform.position.y,sampleCube.transform.position.z);
			listOfVisuals.Add(sampleCube);
		}

		for (int i = 0; i < 100; ++i) {
			GameObject sampleCube = (GameObject)Instantiate (samplePrefab,this.transform.position,this.transform.rotation,this.transform);
			sampleCube.name = "SampleCubereflect" + i;
			//			this.transform.eulerAngles = new Vector3 (0,-0.703125f*i,0);
			//			sampleCube.transform.position = Vector3.right * 100;
			sampleCube.transform.position = new Vector3(sampleCube.transform.position.x-i,sampleCube.transform.position.y,sampleCube.transform.position.z);
			listOfreflectVisuals.Add(sampleCube);
		}

		for (int i = 0; i < 100; ++i) {
			GameObject sampleCube = (GameObject)Instantiate (samplePrefab,this.transform.position,this.transform.rotation,this.transform);
			sampleCube.name = "SampleCubereflect2" + i;
			//			this.transform.eulerAngles = new Vector3 (0,-0.703125f*i,0);
			//			sampleCube.transform.position = Vector3.right * 100;
			sampleCube.transform.position = new Vector3(sampleCube.transform.position.x-i-100,sampleCube.transform.position.y,sampleCube.transform.position.z);
			listOfreflectVisuals2.Add(sampleCube);
		}

		for (int i = 0; i < 200; ++i) {
			GameObject sampleCube = (GameObject)Instantiate (samplePrefab,this.transform.position,this.transform.rotation,this.transform);
			sampleCube.name = "SampleCubereflect3" + i;
			//			this.transform.eulerAngles = new Vector3 (0,-0.703125f*i,0);
			//			sampleCube.transform.position = Vector3.right * 100;
			sampleCube.transform.position = new Vector3(sampleCube.transform.position.x-i-200,sampleCube.transform.position.y,sampleCube.transform.position.z);
			listOfreflectVisuals3.Add(sampleCube);
		}

		listOfreflectVisuals.Reverse ();
	}

	// Update is called once per frame
	void Update () {
		for (int i = 0; i < listOfVisuals.Count; ++i) {
			if (listOfVisuals [i] != null) {
				listOfVisuals [i].transform.localScale = new Vector3 (1,(AudioManager.samples[i] * maxScale) +0.5f,1);

			}
		}

		for (int j = 0; j <listOfreflectVisuals.Count; ++j) {
			if (listOfreflectVisuals [j] != null) {
				listOfreflectVisuals [j].transform.localScale = new Vector3 (1,(AudioManager.samples[10 + j] * maxScale2) +0.5f,1);

			}
		}

		for (int j = 0; j <listOfreflectVisuals2.Count; ++j) {
			if (listOfreflectVisuals2 [j] != null) {
				listOfreflectVisuals2 [j].transform.localScale = new Vector3 (1,(AudioManager.samples[10 + j] * maxScale) +0.5f,1);

			}
		}

		for (int j = 0; j <listOfreflectVisuals3.Count; ++j) {
			if (listOfreflectVisuals3 [j] != null) {
				listOfreflectVisuals3 [j].transform.localScale = new Vector3 (1,(AudioManager.samples[10 + j] * 600) +0.5f,1);

			}
		}


	}
}
