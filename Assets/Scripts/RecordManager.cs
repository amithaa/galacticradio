﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RockVR.Video;
using UnityEngine.UI;

public class RecordManager : MonoBehaviour {

	private static RecordManager _instance;

	public static RecordManager Instance { get { return _instance; } }

	private void Awake ()
	{
		if (_instance != null && _instance != this) {
			Destroy (this.gameObject);
		} else {
			_instance = this;
		}
	}
		

	public GameObject recordingText;

	public string fileName = "";

	public bool recordingActive;

	public AudioSource beepSound;

	private float playback_start_time;

	public Texture2D recordingImage;
	public Texture2D recordImage;

	public Button recordButton;

	Sprite record;
	Sprite recording;


	void Start()
	{
		recording = Sprite.Create (recordingImage, new Rect (0.0f, 0.0f, recordingImage.width, recordingImage.height), new Vector2 (0.5f, 0.5f), 100.0f);
		record = Sprite.Create (recordImage, new Rect (0.0f, 0.0f, recordImage.width, recordImage.height), new Vector2 (0.5f, 0.5f), 100.0f);
	}


	public void StartRecord()
	{
		recordingActive = true;
		recordingText.SetActive (true);
		recordButton.image.sprite = recording;
		fileName = System.DateTime.Now.ToString("MM-dd-yy_hh-mm-ss") + ".mp4"; 
		VideoCaptureCtrl.instance.StartCapture();
		beepSound.Play ();
	}


	public void StopRecordAndReplay()
	{
		StopRecord ();
		VideoController.Instance.PlayVideo();
	}


	public void StopRecord()
	{
		StartCoroutine (PlayBeep());

	}

	IEnumerator PlayBeep()
	{
		beepSound.Play ();

		yield return new WaitForSeconds (1.0f);

		VideoCaptureCtrl.instance.StopCapture ();
		AudioManager.Instance.StopMic ();
		recordButton.image.sprite = record;
		recordingActive = false;
		MainManager.Instance.okToRecord = false;
		recordingText.SetActive (false);
	}



}
