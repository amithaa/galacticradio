﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParamScript : MonoBehaviour {

	public int band;
	public float startScale, scaleMultiplier;

	public bool useBuffer;

	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {
		if (useBuffer == true) {
			transform.localScale = new Vector3 (transform.localScale.x,	(AudioManager.bandBuffer [band] * scaleMultiplier) + startScale, transform.localScale.z);
			if (gameObject.transform.position.x > 300) {
				gameObject.transform.position = new Vector3 (-300,gameObject.transform.position.y,gameObject.transform.position.z);
			}
			gameObject.transform.Translate (Vector3.right * Time.deltaTime * 500);

			GameObject trail = gameObject.transform.GetChild (0).gameObject;
			trail.transform.position = new Vector3(trail.transform.position.x,(AudioManager.bandBuffer [band] * scaleMultiplier) + startScale,trail.transform.position.z);
		}
		if (useBuffer == false) {
			transform.localScale = new Vector3 (transform.localScale.x,	(AudioManager.frequencyBand [band] * scaleMultiplier) + startScale, transform.localScale.z);
			if (gameObject.transform.position.x > 300) {
				gameObject.transform.position = new Vector3 (-300,gameObject.transform.position.y,gameObject.transform.position.z);
			}
			gameObject.transform.Translate (Vector3.right * Time.deltaTime * 500);

			GameObject trail = gameObject.transform.GetChild (0).gameObject;
			trail.transform.position = new Vector3(trail.transform.position.x,(AudioManager.frequencyBand [band] * scaleMultiplier) + startScale,trail.transform.position.z);
		}
	}
}
