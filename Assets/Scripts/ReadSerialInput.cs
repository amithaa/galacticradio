﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;
using System.Threading;
using System;

public class ReadSerialInput : MonoBehaviour
{

	private Thread thread;

	private Queue inputQueue;

	SerialPort stream;

	bool runThread = true;




	void Start ()
	{
		try {
			inputQueue = Queue.Synchronized (new Queue ());
			stream = new SerialPort ("COM8", 9600);
			thread = new Thread (ThreadLoop);
			thread.Start ();
		} catch (System.Exception) {
			Debug.Log ("System is not setup");
		}
	}


	void Update ()
	{
		CheckQueue ();

	}


	void CheckQueue ()
	{
		if (inputQueue.Count != 0) {
			int receivedMessage = (int)inputQueue.Dequeue ();
			if (MainManager.Instance.okToRecord) {
				if (receivedMessage == 1 && !RecordManager.Instance.recordingActive) {
					RecordManager.Instance.StartRecord ();
				} else if (receivedMessage == 2 && RecordManager.Instance.recordingActive) {
					RecordManager.Instance.StopRecordAndReplay ();
				}

			}

		}
	}

	public void ThreadLoop ()
	{
		// Opens the connection on the serial port

		stream.ReadTimeout = 50;
		stream.Open ();

		while (runThread) {
			if (stream.IsOpen) {
				try {
					int receivedMessage = stream.ReadByte ();
					print (receivedMessage);

					if (receivedMessage >= 1 && receivedMessage <= 4)
						inputQueue.Enqueue (receivedMessage);

				} catch (System.Exception) {

				}
			}
		}

	}


	void OnApplicationQuit ()
	{
		stream.Close ();
		runThread = false;
		thread.Join (500);
	}

}
