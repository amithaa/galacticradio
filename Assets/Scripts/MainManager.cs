﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainManager : MonoBehaviour
{

	private static MainManager _instance;

	public static MainManager Instance { get { return _instance; } }

	private void Awake ()
	{
		if (_instance != null && _instance != this) {
			Destroy (this.gameObject);
		} else {
			_instance = this;
		}
	}

	public List<InputField> pinList = new List<InputField> ();
	List<string> pin = new List<string> ();

	string finalPin;

	public GameObject tokenPanel;
	public GameObject recordPanel;
	public GameObject thankYouPanel;
	public GameObject transmitPanel;

	public bool okToRecord;

	JSONParser jp = new JSONParser();

	public Text debugText;

	public string userID;
	public string userMode;

	public bool pinActive;
	public GameObject fbPostBtn;

	// Use this for initialization
	void Start ()
	{
		Application.runInBackground = true;
		pinActive = true;
		pinList [0].Select ();
		pinList [0].ActivateInputField ();
	}


	public void MoveNext (int i)
	{
		if (pinActive == true) {
			pin.Add (pinList [i].text);

			if (i == 3) {
				ValidatePin ();
			} else {
				pinList [i + 1].Select ();
				pinList [i + 1].ActivateInputField ();
			}
		}
	}


	public void ValidatePin ()
	{

		finalPin = string.Join ("", pin.ToArray ());
		print (finalPin);

		string message = DatabaseConnection.Instance.ValidatePin (finalPin);
		print (message);
		userID = jp.ParseJSON (message,out userMode);

		if (userID != "0") {

			okToRecord = true;
			OpenRecordPanel ();
			pinActive = false;

		} else {
			pinActive = false;
			for (int i = 0; i < pinList.Count; ++i) {
				pinList [i].text = "";
			}

			pin.Clear ();
			pinList [0].Select ();
			pinList [0].ActivateInputField ();
			pinActive = true;
		}


		if (userMode != "") {
			if (userMode == "0") {
				fbPostBtn.SetActive (false);
			} else if (userMode == "1") {
				fbPostBtn.SetActive (true);
			}
		}

	}


	public void OpenRecordPanel ()
	{
		tokenPanel.SetActive (false);
		recordPanel.SetActive (true);
		thankYouPanel.SetActive (false);
		transmitPanel.SetActive (false);
		VideoController.Instance.videoPanel.SetActive (false);

		AudioManager.Instance.StartMic ();
	}

	public void SetOkToRecord ()
	{
		okToRecord = true;
	}


	public void OpenThankYouPanel ()
	{
		tokenPanel.SetActive (false);
		recordPanel.SetActive (false);
		thankYouPanel.SetActive (true);
		transmitPanel.SetActive (false);
		VideoController.Instance.videoPanel.SetActive (false);

		SayThanks ();
	}


	public void OpenTokenPanel ()
	{
		tokenPanel.SetActive (true);
		recordPanel.SetActive (false);
		transmitPanel.SetActive (false);
		thankYouPanel.SetActive (false);
		VideoController.Instance.videoPanel.SetActive (false);

	}


	public void OpenTransmitPanel ()
	{
		tokenPanel.SetActive (false);
		recordPanel.SetActive (false);
		transmitPanel.SetActive (true);
		thankYouPanel.SetActive (false);
		VideoController.Instance.videoPanel.SetActive (false);
	}


	//Thank you screen
	public void SayThanks ()
	{
		StartCoroutine (DisableThankYouPanel ());
	}


	//Fetch username and disable thank you
	IEnumerator DisableThankYouPanel ()
	{
		yield return new WaitForSeconds (3.0f);

		thankYouPanel.SetActive (false);


		OpenTokenPanel ();


		//Reset 
		ClearAll ();
	}


	public void ClearAll ()
	{
		okToRecord = false;
		RecordManager.Instance.recordingActive = false;
		RecordManager.Instance.fileName = "";
		for (int i = 0; i < pinList.Count; ++i) {
			pinList [i].text = "";
		}


		pin.Clear ();
		AudioManager.Instance.StopMic ();

		pinList [0].Select ();
		pinList [0].ActivateInputField ();

		pinActive = true;
	}


	public void AlertError (string errorText)
	{
		debugText.gameObject.SetActive (true);
		debugText.text = errorText;
		StartCoroutine (DisableErrorText ());
	}

	IEnumerator DisableErrorText ()
	{
		yield return new WaitForSeconds (10.0f);
		debugText.text = "";
		debugText.gameObject.SetActive (false);

	}


	public void TransmitAndSaveDetails()
	{
		DatabaseConnection.Instance.TransmitMessage (userID,RecordManager.Instance.fileName);
	}


	public void btnCLick()
	{
		print ("Clicked");
	}
}
