﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Net;
using System.IO;
using System.Text;
using System.Collections.Specialized;
using System.Net.Mail;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;


public class DatabaseConnection : MonoBehaviour
{
	private static DatabaseConnection _instance;

	public static DatabaseConnection Instance { get { return _instance; } }


	private void Awake ()
	{
		if (_instance != null && _instance != this) {
			Destroy (this.gameObject);
		} else {
			_instance = this;
		}
	}
		

	// Class to call api
	public string ValidatePin (string _code)
	{
		try {
			var httpWebRequest = (HttpWebRequest)WebRequest.Create ("http://192.168.1.35/galactic_radio/validatecode.php");
			httpWebRequest.ContentType = "application/json; charset=utf-8";
			httpWebRequest.Method = "POST";

			using (var streamWriter = new StreamWriter (httpWebRequest.GetRequestStream ())) {
				string json = "{\"code\":\"" + _code + "\"}";

				streamWriter.Write (json);
				streamWriter.Flush ();
				streamWriter.Close ();

			}

			var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse ();
			using (var streamReader = new StreamReader (httpResponse.GetResponseStream ())) {
				var data = streamReader.ReadToEnd ();
				print(data);
				return data.ToString ();
			}

		} catch (System.Exception e) {
			// Your catch here
			print (e);
			MainManager.Instance.AlertError (e.ToString());

			return "";
		}
	}


	public string TransmitMessage (string _userid, string _videoname)
	{
		try {
			var httpWebRequest = (HttpWebRequest)WebRequest.Create ("http://192.168.1.45/galactic_radio/updadetails.php");
			httpWebRequest.ContentType = "application/json; charset=utf-8";
			httpWebRequest.Method = "POST";

			using (var streamWriter = new StreamWriter (httpWebRequest.GetRequestStream ())) {
				string json = "{\"userid\":\"" + _userid + "\",\"videoname\":\"" + _videoname + "\"}";

				streamWriter.Write (json);
				streamWriter.Flush ();
				streamWriter.Close ();

			}

			var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse ();
			using (var streamReader = new StreamReader (httpResponse.GetResponseStream ())) {
				var data = streamReader.ReadToEnd ();
				print(data);
				return data.ToString ();
			}

		} catch (System.Exception e) {
			// Your catch here
			print (e);
			MainManager.Instance.AlertError (e.ToString());

			return "";
		}
	}


}
