﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[RequireComponent (typeof(AudioSource))]
public class AudioManager : MonoBehaviour
{

	private static AudioManager _instance;

	public static AudioManager Instance { get { return _instance; } }

	private void Awake ()
	{
		if (_instance != null && _instance != this) {
			Destroy (this.gameObject);
		} else {
			_instance = this;
		}
	}


	AudioSource audioSource;

	public AudioClip audioClip;

	//Microphone
	public bool useMicrophone;
	public string selectedDevice;

	public static float[] samples = new float[512];

	public static float[] frequencyBand = new float[8];

	public static float[] bandBuffer = new float[8];

//	float[] bufferDecrease = new float[8];


	//	public AudioMixerGroup mixerGrouMicrophone, mixerGroupMaster;

	// Use this for initialization
	void Start ()
	{
		audioSource = GetComponent<AudioSource> ();

	}


	public void StartMic ()
	{
		if (useMicrophone) {
			if (Microphone.devices.Length > 0) {
				selectedDevice = Microphone.devices [0].ToString ();
				print (selectedDevice);
				audioSource.clip = Microphone.Start (selectedDevice, true, 10, AudioSettings.outputSampleRate);
				audioSource.loop = true;

				if (Microphone.IsRecording (selectedDevice)) {
					while (!(Microphone.GetPosition (selectedDevice) > 0)) {
					}

				} else {
					Debug.Log ("Doesnt work\t");
				}
			} else {
				useMicrophone = false;
			}

		} else {
			audioSource.clip = audioClip;
		}

		audioSource.Play ();
	}

	public void StopMic()
	{
		audioSource.Stop ();
		Microphone.End (selectedDevice);
	}
	
	// Update is called once per frame
	void Update ()
	{
		GetSpectrumAudioSource ();
//		MakeFrequencyBands ();
//		BandBuffer ();
	}


//	void BandBuffer ()
//	{
//		for (int i = 0; i < 8; ++i) {
//
//			if (frequencyBand [i] > bandBuffer [i]) {
//				bandBuffer [i] = frequencyBand [i];
//				bufferDecrease [i] = 0.005f;
//			}
//
//			if (frequencyBand [i] < bandBuffer [i]) {
//				bandBuffer [i] -= bufferDecrease [i];
//				bufferDecrease [i] *= 1.2f;
//			}
//		}
//	}


	void GetSpectrumAudioSource ()
	{
		audioSource.GetSpectrumData (samples, 0, FFTWindow.BlackmanHarris);
//		audioSource.GetOutputData(samples,0);
	}
//
//
//	void MakeFrequencyBands ()
//	{
//		int count = 0;
//		float average = 0;
//
//		for (int i = 0; i < 8; ++i) {
//			int sampleCount = (int)Mathf.Pow (2, i) * 2;
//
//			if (i == 7) {
//				sampleCount += 2; 
//			}
//
//			for (int j = 0; j < sampleCount; ++j) {
//				average += samples [count] * (count + 1);
//				count++;
//			}
//
//			average /= count;
//
//			frequencyBand [i] = average * 10;
//		}
//	}
}
