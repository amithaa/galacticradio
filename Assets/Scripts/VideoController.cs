﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;

public class VideoController : MonoBehaviour
{

	private static VideoController _instance;

	public static VideoController Instance { get { return _instance; } }

	private void Awake ()
	{
		if (_instance != null && _instance != this) {
			Destroy (this.gameObject);
		} else {
			_instance = this;
		}
	}


	public VideoPlayer videoPlayer;

	private AudioSource audioSource;

	public RawImage ri;

	public GameObject videoPanel;
	public GameObject pleaseWaitPanel;

	//
	//	void Start()
	//	{
	//		PlayVideo ();
	//	}
	//
	public void PlayVideo ()
	{
		StartCoroutine (StartVideo ());
	}

	IEnumerator StartVideo ()
	{

		pleaseWaitPanel.SetActive (true);
		ri.color = Color.black;

		while (RockVR.Video.VideoCaptureCtrl.instance.status != RockVR.Video.VideoCaptureCtrl.StatusType.FINISH) {
			yield return new WaitForSeconds (1);
		}

		pleaseWaitPanel.SetActive (false);
		ri.gameObject.SetActive (true);
		videoPanel.SetActive (true);


		//Add VideoPlayer to the GameObject
		videoPlayer = gameObject.AddComponent<VideoPlayer> ();

		//Add AudioSource
		audioSource = gameObject.AddComponent<AudioSource> ();

		//Disable Play on Awake for both Video and Audio
		videoPlayer.playOnAwake = false;
		audioSource.playOnAwake = false;
		audioSource.Pause ();


		// Vide clip from Url
		if (System.IO.File.Exists (RockVR.Video.PathConfig.saveFolder + RecordManager.Instance.fileName)) {
			videoPlayer.source = VideoSource.Url;
			videoPlayer.url = RockVR.Video.PathConfig.saveFolder + RecordManager.Instance.fileName;
		} else {
			MainManager.Instance.AlertError ("THERE WAS AN ERROR DURING VIDEO CAPTURE. PLEASE RETAKE VIDEO");
			StopVideo ();
			MainManager.Instance.OpenRecordPanel ();
			MainManager.Instance.SetOkToRecord ();
		}

	

		//Set Audio Output to AudioSource
		videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;

		videoPlayer.controlledAudioTrackCount = 1;

		//Assign the Audio from Video to AudioSource to be played
		videoPlayer.EnableAudioTrack (0, true);
		videoPlayer.SetTargetAudioSource (0, audioSource);

		videoPlayer.errorReceived += VideoPlayer_errorReceived;

//		videoPlayer.clip = videoToPlay;
		videoPlayer.Prepare ();

		//Wait until video is prepared
		WaitForSeconds waitTime = new WaitForSeconds (1);
		while (!videoPlayer.isPrepared) {
			//Debug.Log("Preparing Video");
			//Prepare/Wait for 5 sceonds only
			yield return waitTime;
			//Break out of the while loop after 5 seconds wait
			break;
		}

		Debug.Log ("Done Preparing Video");

		//Assign the Texture from Video to RawImage to be displayed
		ri.texture = videoPlayer.texture;
		ri.color = Color.white;

		videoPlayer.Play ();

		//Play Sound
		audioSource.Play ();

		audioSource.volume = 0.8f;

		Debug.Log ("Playing Video");
		while (videoPlayer.isPlaying) {
			Debug.LogWarning ("Video Time: " + Mathf.FloorToInt ((float)videoPlayer.time));
			yield return null;
		}


		StopVideo ();
	}


	private void VideoPlayer_errorReceived (VideoPlayer source,string message) {
		MainManager.Instance.AlertError ("THERE WAS AN ERROR DURING VIDEO CAPTURE. PLEASE RETAKE VIDEO");
		videoPlayer.errorReceived -= VideoPlayer_errorReceived;//Unregister to avoid memory leaks
		StopVideo();
//		MainManager.Instance.SetOkToRecord ();
//		MainManager.Instance.OpenRecordPanel ();
	}

	void EndReached (UnityEngine.Video.VideoPlayer vp)
	{
		StopVideo ();
	}

	public void StopVideo ()
	{
//		videoPanel.SetActive (false);
		ri.gameObject.SetActive (false);
		videoPlayer.Stop ();
		audioSource.Stop ();

	}

}